# Atividade referente a vaga de DBA Jr - [Automatos](https://automatos.com/)

## Cenário

Uma das funcionalidades do agente da Automatos é a capacidade de coletar informações referentes à geolocalização de uma máquina. Para acomodar essa funcionalidade, duas novas tabelas foram criadas no banco de dados. 

A primeira, *LAST_GEO*, armazena somente a última posição coletada das máquinas. Além do id da máquina (campo *MAQUINA*), também são armazenados a *LONGITUDE* e *LATITUDE*. O último campo *ENTRADA* indica o dia (e.g. *2020-01-29*) em que a máquina entrou nessa posição.

A segunda tabela *HIST_GEO* serve como uma tabela histórico. Através de um trigger, sempre que uma máquina troca de posição geográfica, automaticamente a linha referente na tabela *LAST_GEO* é atualizada com a nova posição e uma nova linha é inserida na tabela *HIST_GEO*, armazenando a *MAQUINA*, a *LATITUDE*, *LONGITUDE* e *ENTRADA* da posição anterior, assim como a *SAIDA* da posição anterior (i.e., o último dia que a máquina ficou naquela posição).

Uma outra tabela já existente, *INFO*, armazena informações detalhadas das máquinas. As informações são o id da máquina (coluna *MAQUINA*), *USUARIO* que utiliza, o sistema operacional (*S_OPERACIONAL*) e quanto de memória em GB a máquina possui (coluna *MEMORIA_GB*).

Observações:

1. Devido a alguma instabilidade na placa de rede da máquina, algumas coletas podem vir inválidas. Nesse caso, é inserido no banco o valor 0 para latitude e longitude daquela coleta. Assim, entende-se como coleta válida, as coletas com valor de latitude e longitude diferentes de 0.

2. Entende-se como máquina da diretoria, as máquinas com ID (campo *MAQUINA*) terminado em "A" (e.g., *XXA*, *ZZA*, *PPA*, etc.).

3. Podem existir máquinas na tabela **INFO** sem coletas de geolocalização (tabelas **LAST_GEO** e **HIST_GEO**). Assim como podem ter máquinas mencionadas em coletas de geolocalização (tabelas **LAST_GEO** e **HIST_GEO**) sem informações (tabela **INFO**).

## Atividade

Existem as seguintes demandas por informações referentes a funcionalidade de geolocalização:

1. Somente a data de entrada da coleta **válida** mais antiga da tabela histórico **HIST_GEO** de cada máquina (campos *ENTRADA* e *MAQUINA*).

2. Todas as coletas (campos *ENTRADA*, *SAIDA*, *MAQUINA*, *LATITUDE*, *LONGITUDE*) **válidas** da tabela histórico **HIST_GEO** das máquinas que **não sejam da diretoria**. Considerar apenas as máquinas com informação na tabela **INFO** (e.g. se não tiver uma linha para máquina na tabela **INFO**, não exibir aquela máquina na consulta final).

3. Para todas as máquinas com informações na tabela **INFO**, exibir a latitude e longitude atual (tabela **LAST_GEO**), assim como o nome do usuário e sistema operacional da máquina (campos *MAQUINA*, *USUARIO*, *S_OPERACIONAL*, *LATITUDE*, *LONGITUDE*). Exibir mesmo se a coleta da latitude/longitude for inválida. Mesmo se não tiver coleta para alguma máquina na tabela **INFO**, também exibir ela na consulta final (os campos latitude/longitude deverão ser exibidos como NULL).

4. Onde cada máquina estava na virada do ano novo (01/01/2020). Mostrando os campos MAQUINA, LATITUDE e LONGITUDE para cada máquina. Considere, por exemplo, se uma máquina entrou em uma posição geográfica no dia 01/01/2020, ela passou a virada nesta posição geográfica. Do mesmo modo, se a máquina saiu de uma posição geográfica no dia 01/01/2020, ela também passou a virada do ano nesta posição geográfica.

5. Somente a última posição geográfica de cada máquina, considerando o dado mais atual **válido** (*LATITUDE* e *LONGITUDE* diferentes de 0) em uma união das tabelas **HIST_GEO** e **LAST_GEO**. Mostrando quando foi o horário de início (*ENTRADA*) e fim (*SAIDA*, NULL se é a coleta mais atual) da coleta, a *LATITUDE*, a *LONGITUDE* e o id da máquina (*MAQUINA*).

Assim, **crie uma query SQL para cada item acima**.

O arquivo [CREATE.sql](CREATE.sql) contém a DDL necessária para criação das 3 tabelas referentes ao cenário (desconsidere os triggers mencionados).

Os arquivos [LAST_GEO.csv](LAST_GEO.csv), [HIST_GEO.csv](HIST_GEO.csv) e [INFO.csv](INFO.csv) contém dados em formato CSV para popular as respectivas tabelas, que devem ser utilizados para testes.

## Envio

Para cada item da atividade, criar um arquivo .sql com a query SQL criada. Indicar no nome do arquivo a qual item se refere. Caso necessário, criar um arquivo readme.txt contendo dúvidas, observações e comentários sobre a atividade.

Esses arquivos devem ser anexados em um único arquivo e enviado para o email adilson.rapozo@automatos.com

## Dicas

Para testar seus SELECTs, pode-se utilizar o seguinte site [https://www.db-fiddle.com/](https://www.db-fiddle.com/). 

Basta colar o conteúdo do arquivo [CREATE.sql](CREATE.sql) no espaço da esquerda, para criação do ambiente de teste; colar seu SELECT pronto no espaço da direita; e executar.

O diretório [resultados/](resultados/) contém as saídas esperadas de cada item.
